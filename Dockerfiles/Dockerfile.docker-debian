ARG BASE_IMAGE
FROM ${BASE_IMAGE}

# Spécifiques à cet image
LABEL Vendor="Actilis" \
      Maintainer="Francois MICAUX <dok-images@actilis.net>" \
      License=GPLv3 

# Configure locales
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends curl locales; \
      rm -rf /var/lib/apt/lists/*; \
      sed -i -e '/fr_FR.UTF-8/s/^# //g' -e '/en_US.UTF-8/s/^# //g' /etc/locale.gen && locale-gen

# Capabilities for chown ==> enables "choose-uuid" feature when non-root
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends libcap2-bin; \
    setcap cap_chown=ep /bin/chown; \
    apt-get remove -y libcap2-bin; \
    rm -rf /var/lib/apt/lists/*;

# Ensure Default Username and Groupname are present
ENV WEB_USERNAME www-data
ENV WEB_GROUPNAME www-data

RUN set -eux \
 && grep -q ${WEB_GROUPNAME} /etc/group || addgroup -g ${WEB_GID:=33} ${WEB_GROUPNAME}

RUN set -eux \
 && grep -q ${WEB_USERNAME} /etc/passwd || adduser -D -u ${WEB_UID:=33} -G ${WEB_GROUPNAME} ${WEB_USERNAME}


# Config d'apache
# Env vars nécessaires pour les scripts d'EP
# ==========================================
ENV HTTPD_LISTEN_PORT 80
ENV CONF_PATH /usr/local/apache2
ENV HTTPD_CONF_FILE ${CONF_PATH}/conf/httpd.conf

ENV MODULES_PATH /usr/local/apache2/modules
ENV LOADMODULE_FILE ${CONF_PATH}/conf.modules.d/00-docker-modules.conf
ENV RUN_DIR /var/run/apache2

ENV LDAP_CONF /etc/ldap/ldap.conf



### HTTPD Configfiles
RUN  set -eux \
 && install -d -g ${WEB_GROUPNAME} -m 2775 ${CONF_PATH}/conf/ \
                                           ${CONF_PATH}/conf.modules.d \
                                           ${CONF_PATH}/conf.d \
                                           ${CONF_PATH}/logs \
 && sed -i \
    -e "/Listen 80$/s,80,_HTTPD_LISTEN_PORT_," \
    -e '/^LoadModule/s,^,#,' \
    -e '/LoadModule foo_module/aIncludeOptional ${CONF_PATH}/conf.modules.d/*.conf' \
    ${HTTPD_CONF_FILE} \
 && touch ${LOADMODULE_FILE} \
 && echo "Include conf.d/*.conf" >> ${HTTPD_CONF_FILE}


# config 
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} httpd-vhost.conf   ${CONF_PATH}/conf.d/vhost.conf

# Scripts de l'EP
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} ep-httpd.sh          /ep.d/00-httpd.sh
COPY --chown=${WEB_USERNAME}:${WEB_GROUPNAME} ep.sh                /ep.sh
RUN set -eux \
 && chmod -R 755 /ep.sh /ep.d/

RUN set -eux \
 && mkdir -p /var/www/html /var/run/apache2 \
 && chmod o+x /var /var/www \
 && chown -R ${WEB_USERNAME}.${WEB_GROUPNAME} /var/www/html \
 && chgrp -R                 ${WEB_GROUPNAME} /var/run/apache2 \
 && chmod -R 755 /var/www/html


### LDAP config
RUN  set -eux \
 && sed -i -e "\$a\
TLS_REQCERT      ${LDAP_TLS_REQCERT:-never}" ${LDAP_CONF}


### Ports
EXPOSE ${HTTPD_LISTEN_PORT}

## Entrypoint calls all scripts in /ep.d/*.sh (they apply the configuration at statup)
ENTRYPOINT ["/ep.sh"]

## ep.sh ends with "exec $@" 
CMD ["httpd", "-D", "FOREGROUND"]

# ## Default dir for apps
WORKDIR /var/www/html

