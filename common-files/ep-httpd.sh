#!/bin/bash

myName=EP-HTTPD

function _echo()
{
	[ "${DEBUG}" == "true" ] && echo "[$myName]: $@"
}

function a2enmod()
{
  for MOD in "$@" ; do
      [ ! -f ${MODULES_PATH}/mod_${MOD}.so ] && echo "ERROR: unknown module ${MOD}. Check HTTPD_LOAD_MODULES env var." && exit 1
      grep -q "${MOD}_module" ${LOADMODULE_FILE} || echo "LoadModule ${MOD}_module modules/mod_${MOD}.so" >> ${LOADMODULE_FILE}
  done
}

# Local vars & paths
GLOBAL_CONF_FILE=${CONF_PATH}/conf.d/httpd-global.conf
VHOST_CONF_FILE=${CONF_PATH}/conf.d/vhost.conf
DEFINES=""

# UID
myUID=$(id -u)

# GID : don't stay with GID=0 if UID is given
myGID=$(id -g); [ ${myGID} = 0 ] && myGID=${myUID}

echo "[$myName] running with UID ${myUID}/${myGID}..." >/dev/stderr

# Enable choose-uuid (WARNING: cap_chown=ep on /bin/chown)
chown    ${myUID}:${myGID} ${CONF_PATH} ${CONF_PATH}/logs ${RUN_DIR}
chown -R ${myUID}:${myGID} ${CONF_PATH}/conf* 


# Some default values
HTTPD_ENABLE_PHPFPM_INET=${HTTPD_ENABLE_PHPFPM_INET:-false}
HTTPD_LISTEN_PORT=${HTTPD_LISTEN_PORT:-80}
PHPFPM_HOST=${PHPFPM_HOST:-phpfpm}
PHPFPM_PORT=${PHPFPM_PORT:-9000}

# Sec Tuning
HTTPD_GLOBAL__ServerTokens=${HTTPD_GLOBAL__ServerTokens:-prod}
HTTPD_GLOBAL__ServerSignature=${HTTPD_GLOBAL__ServerSignature:-Off}
HTTPD_GLOBAL__FileETag=${HTTPD_GLOBAL__FileETag:-None}
HTTPD_GLOBAL__EnableSendfile=${HTTPD_GLOBAL__EnableSendfile:-Off}
HTTPD_GLOBAL__TraceEnable=${HTTPD_GLOBAL__TraceEnable:-Off}

# Keepalive
HTTPD_GLOBAL__KeepAlive=${HTTPD_GLOBAL__KeepAlive:-On}
HTTPD_GLOBAL__KeepAliveTimeout=${HTTPD_GLOBAL__KeepAliveTimeout:-2}
HTTPD_GLOBAL__MaxKeepAliveRequests=${HTTPD_GLOBAL__MaxKeepAliveRequests:-100}

# Timeouts
HTTPD_GLOBAL__Timeout=${HTTPD_GLOBAL__Timeout:-5}
HTTPD_GLOBAL__ProxyTimeout=${HTTPD_GLOBAL__ProxyTimeout:-300}

# MPM tuning : event with up to 1500 slots
HTTPD_MPM=${HTTPD_MPM:-event}
case "${HTTPD_MPM}" in
  prefork)
      HTTPD_GLOBAL__StartServers=${HTTPD_GLOBAL__StartServers:-10}
      HTTPD_GLOBAL__MinSpareServers=${HTTPD_GLOBAL__MinSpareThreads:-10}
      HTTPD_GLOBAL__MaxSpareServers=${HTTPD_GLOBAL__MaxSpareThreads:-50}
      HTTPD_GLOBAL__MaxRequestWorkers=${HTTPD_GLOBAL__MaxRequestWorkers:-250}
      ;;
  worker|event)
      HTTPD_GLOBAL__StartServers=${HTTPD_GLOBAL__StartServers:-2}
      HTTPD_GLOBAL__ServerLimit=${HTTPD_GLOBAL__ServerLimit:-32}
      HTTPD_GLOBAL__ThreadsPerChild=${HTTPD_GLOBAL__ThreadsPerChild:-50}
      HTTPD_GLOBAL__MinSpareThreads=${HTTPD_GLOBAL__MinSpareThreads:-75}
      HTTPD_GLOBAL__MaxSpareThreads=${HTTPD_GLOBAL__MaxSpareThreads:-250}
      HTTPD_GLOBAL__MaxRequestWorkers=${HTTPD_GLOBAL__MaxRequestWorkers:-1500}
      ;;
esac

# Serverstatus
HTTPD_ENABLE_STATUS=${HTTPD_ENABLE_STATUS:-true}
HTTPD_STATUS_URI=${HTTPD_STATUS_URI:-/status}


# logs to container output
sed -i -e '/ErrorLog.*logs.error.log/s,logs/error.log,/proc/1/fd/2,' \
       -e '/CustomLog .*logs.access.log.*combined/s,logs/access.log,/proc/1/fd/1,' \
       ${HTTPD_CONF_FILE}

# Listen PORT
sed -i \
    -e "/^Listen/s,_HTTPD_LISTEN_PORT_,${HTTPD_LISTEN_PORT}," \
    ${HTTPD_CONF_FILE}


# Paramétrage identité numérique seulement si on est pas lancé en tant que root
if [ ${myUID} != 0 ]; then
    sed -i \
    -e "/^User ${WEB_USERNAME}/s,${WEB_USERNAME},#${myUID},g" \
    -e "/^Group ${WEB_GROUPNAME}/s,${WEB_GROUPNAME},#${myGID},g" \
    ${HTTPD_CONF_FILE}
else
    sed -i \
    -e "/^User /s,^.*$,User ${WEB_USERNAME}," \
    -e "/^Group /s,^.*$,Group ${WEB_GROUPNAME}," \
    ${HTTPD_CONF_FILE}
fi

# ===================
# Print startup infos
# ===================
_echo "Vars :"
_echo -e "Webserver parameters :
     WEB_APP_DIR=${WEB_APP_DIR:-} (DocumentRoot --> /var/www/html par défaut),
     WEB_APP_INDEX=${WEB_APP_INDEX:=index.php index.html} (DirectoryIndex)
"

# TUNING -> Default MPM : event
_echo "** HTTPD_MPM : [1m${HTTPD_MPM:=event}[0m"

# PHPFPM
_echo -e "\n** HTTPD_ENABLE_PHPFPM_INET : [1m${HTTPD_ENABLE_PHPFPM_INET:=false}[0m"
if [ "${HTTPD_ENABLE_PHPFPM_INET}" == "true" ] ; then
  _echo "**   PHPFPM_HOST=[1m${PHPFPM_HOST:=phpfpm}[0m"
  _echo "**   PHPFPM_PORT=[1m${PHPFPM_PORT:=9000}[0m"
fi

# CGI
_echo -e "\n** HTTPD_ENABLE_CGI : [1m${HTTPD_ENABLE_CGI:=false}[0m"
_echo ""

# WEBDAV
_echo -e "\n** HTTPD_ENABLE_WEBDAV : [1m${HTTPD_ENABLE_WEBDAV:=false}[0m"
_echo ""


# ==========================
# == Paramétrage de HTTPD ==
# ==========================
# MPM (if MPM=event is specified, use it, else use "event" as default)
case "${HTTPD_MPM}" in
   prefork|worker|event) 
            a2enmod mpm_${HTTPD_MPM}
        		;;
   *)       echo "Err: MPM should be 'event', 'worker' or 'prefork', not '$HTTPD_MPM'"
            exit 1
esac


# All modules are disabled by default : enable some mandatory modules
a2enmod unixd authz_core authz_host deflate dir env filter headers log_config mime negotiation setenvif version

# ==================================
# HTTPD_DISABLE_HEADER_XFRAMEOPTIONS
# ==================================
if [ "${HTTPD_DISABLE_HEADER_XFRAMEOPTIONS:-false}" == true ] ; then
	    DEFINES="${DEFINES} -D DISABLE_HEADER_XFRAMEOPTIONS"
fi


# ======================
# HTTPD_SHOW_SERVER_LOAD
# ======================
if [ "${HTTPD_SHOW_SERVER_LOAD}" == true ] ; then
	    DEFINES="${DEFINES} -D SHOW_SERVER_LOAD"
fi


# ==============================
# HTTPD_DISABLE_STDOUT_ACCESSLOG
# ==============================
if [ "${HTTPD_DISABLE_STDOUT_ACCESSLOG}" == true ] ; then
	    DEFINES="${DEFINES} -D DISABLE_STDOUT_ACCESSLOG"
fi

# ======================
# Sommes_nous un proxy ?
# ======================
set | grep -q -E '^HTTPD_.*proxy_|^' && a2enmod proxy


# ===========================================
# HTTPD_ENABLE_PHPFPM is true ==> Activate it
# ===========================================
if [ "${HTTPD_ENABLE_PHPFPM_INET}" == true ] ; then
	    DEFINES="${DEFINES} -D ENABLE_PHPFPM_INET"
	    export PHPFPM_HOST PHPFPM_PORT
      a2enmod slotmem_shm proxy proxy_fcgi lbmethod_byrequests
fi

# ===================================================
# HTTPD_SIMPLE_HTTP_PROXYPASS is true ==> Activate it
# ===================================================
if [ "${HTTPD_SIMPLE_HTTP_PROXYPASS}" == true ] ; then
	    DEFINES="${DEFINES} -D ENABLE_SIMPLE_HTTP_PROXYPASS"
      a2enmod proxy proxy_http
      sed -i -e "/_PROXYPASS_URI_/s,_PROXYPASS_URI_,${PROXYPASS_URI:-/}," \
             -e "/_PROXY_PRESERVE_HOST_/s,_PROXY_PRESERVE_HOST_,${PROXY_PRESERVE_HOST:-Off}," \
             -e "/_PROXYPASS_TARGET_/s,_PROXYPASS_TARGET_,${PROXYPASS_TARGET:-http://www.perdu.com/}," \
          ${VHOST_CONF_FILE}
fi

# ===============================================
# HTTPD_ENABLE_WEBDAV is true ==> Activate WEBDAV
# ===============================================
if [ "${HTTPD_ENABLE_WEBDAV}" == true ] ; then
    DEFINES="${DEFINES} -D ENABLE_WEBDAV"
    a2enmod dav dav_fs dav_lock
fi

# =========================================
# HTTPD_ENABLE_CGI is true ==> Activate CGI
# =========================================
if [ "${HTTPD_ENABLE_CGI}" == true ] ; then
    DEFINES="${DEFINES} -D ENABLE_CGI"
    case "${HTTPD_MPM}" in
      worker|event) 
             a2enmod cgid
             ;;
      prefork) 
             a2enmod cgi
             ;;
      *)       echo "Err: MPM should be 'event', 'worker' or 'prefork', not '$HTTPD_MPM'"
               exit 1
    esac
    # ======================
    # Suffixes for CGI files
    # ======================
    sed -i -e "/_CGISUFFIXES_/s,_CGISUFFIXES_,${HTTPD_CGI_SUFFIXES:-cgi sh}," \
          ${VHOST_CONF_FILE}

fi

# ================================================
# HTTPD_ENABLE_MOD_REWRITE is true ==> Activate it
# ================================================
if [ "${HTTPD_ENABLE_MOD_REWRITE:-true}" == true ] || [ "${HTTPD_ENABLE_STATUS}" == true ] ; then
   a2enmod rewrite
fi


# ===========================================
# HTTPD_ENABLE_STATUS is true ==> Activate it
# ===========================================
if [ "${HTTPD_ENABLE_STATUS}" == true ] ; then

     a2enmod status

	   DEFINES="${DEFINES} -D ENABLE_STATUS"
      sed -i -e "/_STATUS_URI_/s,_STATUS_URI_,${HTTPD_STATUS_URI:=/status}," \
             -e "/_STATUS_ALLOWED_IP_/s,_STATUS_ALLOWED_IP_,${HTTPD_STATUS_ALLOWED_IP:=127.0.0.1}," \
          ${VHOST_CONF_FILE}
      if [ -n "${HTTPD_STATUS_ALLOWED_X_FW_F_IP}" ]; then
        sed -i "s/_STATUS_ALLOWED_X_FW_F_IP_/${HTTPD_STATUS_ALLOWED_X_FW_F_IP}/" ${VHOST_CONF_FILE}
      else
        # sed -i "/_STATUS_ALLOWED_X_FW_F_IP_/d" ${VHOST_CONF_FILE}
        sed -i "/AllowIP/d" ${VHOST_CONF_FILE}
      fi
      # Remove authz_host if "any" ip is allowed
      if [ "${HTTPD_STATUS_ALLOWED_IP}" == "any" ] ; then
        sed -i -e "/Require ip any/d" ${VHOST_CONF_FILE}
      fi
fi


# ==================================
# Add other modules asked by env VAR
# ==================================
a2enmod ${HTTPD_LOAD_MODULES}


# =======================================================
# Paramétrage VirtualHost : DocumentRoot + DirectoryIndex
# =======================================================
sed -i -e "/_DOCUMENTROOT_/s,_DOCUMENTROOT_,/var/www/html/${WEB_APP_DIR}," \
       -e "/_DIRECTORYINDEX_/s,_DIRECTORYINDEX_,${WEB_APP_INDEX}," \
       -e "/_DOCROOT_EXTRA_OPTIONS_/s,_DOCROOT_EXTRA_OPTIONS_,${DOCROOT_EXTRA_OPTIONS:-}," \
          ${VHOST_CONF_FILE}

# ========================
# Méthodes HTTP autorisées
# ========================
# - Si ALL, on supprime la ligne de config
if [ "${HTTPD_ALLOWED_METHODS:-HEAD GET POST}" == "ALL" ] ; then
   sed -i -e "/AllowMethods/d"  ${VHOST_CONF_FILE}
else
# - Sinon, on charge le module et on adapte config
   a2enmod allowmethods
   sed -i -e "/_HTTPMETHODS_/s,_HTTPMETHODS_,${HTTPD_ALLOWED_METHODS:-HEAD GET POST}," \
          ${VHOST_CONF_FILE}
fi

# =======================================================
# Paramétrage du serveur : httpd-global.conf
# =======================================================
set | grep "^HTTPD_GLOBAL__" | while read L ; do
 echo "${L#HTTPD_GLOBAL__}" | sed -e 's,=, ,'
done > ${GLOBAL_CONF_FILE}


# = Fix owner of web-content
[ "${FIX_WEBCONTENT_OWNER}" == "true" ] && chown -R ${myUID}:${myGID} /var/www/html

# = En DEV, si on a pas de contenu, on en crée un.
[ $(ls -a /var/www/html | wc -w) == 2 ] \
  && echo "[$myName] /var/www/html is empty, adding an index.html" >/dev/stderr \
  && chown ${myUID}:${myGID} /var/www/html && echo "Servi par Apache." > /var/www/html/index.html


# Do the job !
IP=$(hostname -i | cut -d" " -f1)

_echo ""
_echo -e "======================================="
_echo "   Listening to http://${IP}:${HTTPD_LISTEN_PORT}/"
_echo "   DocumentRoot is /var/www/html/${WEB_APP_DIR}."
_echo "   Please tune /var/www/html/.htaccess to your needs."
_echo -e "======================================="


