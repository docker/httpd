#!/bin/bash

function _echo()
{
	[ "${DEBUG}" == "true" ] && echo "$@"
}


# Preparation HTTPD + HTTPD + ... whatever the app adds
for S in /ep.d/*.sh
do
    . $S
done

_echo "DEFINES=${DEFINES}"

# Lancement de HTTPD (sauf si une CMD différente est spécifiée)
if [ "$1" == "httpd" ]; then
  exec "$@" ${DEFINES}
else
  _echo "ARGS=$@"
  exec "$@"
fi
